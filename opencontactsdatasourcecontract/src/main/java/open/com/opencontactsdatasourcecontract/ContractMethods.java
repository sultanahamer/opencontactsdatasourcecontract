package open.com.opencontactsdatasourcecontract;

import static java.util.Arrays.asList;

public class ContractMethods {
    public static ContractMethod fetchAllNamesAndPhoneNumbersV1 = new ContractMethod("fetchAllNamesAndPhoneNumbers", "1", asList(Contract.Permissions.READ_ALL_NAME_AND_PHONE_NUMBERS));
    public static ContractMethod fetchNamesForPhoneNumbersV1 = new ContractMethod("fetchNamesForPhoneNumbers", "1", asList(Contract.Permissions.READ_ALL_NAME_AND_PHONE_NUMBERS));
}
