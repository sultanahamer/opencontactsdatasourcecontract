package open.com.opencontactsdatasourcecontract;

import java.util.List;

public class ContractMethod {
    public String version;
    public String name;

    public ContractMethod(String name, String version, List<String> permissionsRequired) {
        this.version = version;
        this.name = name;
        this.permissionsRequired = permissionsRequired;
    }

    public static String methodKey(String name, String version) {
        return name + "-" + version;
    }
    public static String methodKey(ContractMethod contractMethod) {
        return methodKey(contractMethod.name, contractMethod.version);
    }

    public List<String> permissionsRequired;
}
