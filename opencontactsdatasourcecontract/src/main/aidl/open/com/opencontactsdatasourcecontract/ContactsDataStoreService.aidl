// ContactsDataService.aidl
package open.com.opencontactsdatasourcecontract;

// Declare any non-default types here with import statements

interface ContactsDataStoreService {
    String call(String authCode, String args);
}