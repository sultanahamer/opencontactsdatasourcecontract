package open.com.opencontactsdatasourcecontract;

import java.util.List;

public class Contract {
    public interface Permissions {
        public static String READ_ALL = "READ_ALL";
        public static String WRITE_ALL = "WRITE_ALL";
        public static String READ_NAME_FOR_GIVEN_PHONE_NUMBER = "READ_NAME_FOR_GIVEN_PHONE_NUMBER";
        public static String READ_ALL_NAME_AND_PHONE_NUMBERS = "READ_ALL_NAME_AND_PHONE_NUMBERS";
    }

    public interface PermissionsActivity {
        public static String PERMISSIONS_EXTRA = "PERMISSIONS";
        public static String LAUNCH_ACTION = "OPENCONTACTS_DATA_PERMISSION";
        public static String RESULT_AUTH_CODE = "RESULT_AUTH_CODE";
    }

    public static String failureSignature = "Failed";

    public static boolean isErrorResponseV1(String msg) {
        String[] responseSplit = msg.split("\n");
        return responseSplit.length > 0 && failureSignature.equals(responseSplit[0]);
    }

    public static String getErrorMessageV1(String msg) {
        String[] responseSplit = msg.split("\n");
        return responseSplit.length > 1 ? responseSplit[1] : "";
    }

    public static String formErrorResponseV1(String msg) {
        return failureSignature + "\n" + msg;
    }

    public static void hasEnoughPermissions(List<ContractMethod> methodsToCall, List<String> permissionsAvailable) {

    }

}
